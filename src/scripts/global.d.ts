interface IFighter {
  _id: string;
  name: string;
  source: string;
}

interface IFighterDetails extends IFighter {
  health: number;
  attack: number;
  defense: number;
}

interface IPLayer extends IFighterDetails {
  currentHealth: number;
  block: boolean;
  canUseCombo: boolean;
  indicator: string;
  comboStatus: string;
  fightText: string;
}

interface IModal {
  title: string;
  bodyElement: HTMLElement;
}

interface IDomElem {
  tagName: string;
  className: string;
  attributes?: { [key: string]: string };
}
