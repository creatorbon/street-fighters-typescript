import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters(): Promise<IFighterDetails[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as Array<IFighterDetails>;
    } catch (error) {
      throw new Error(error);
    }
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighterDetails;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export const fighterService = new FighterService();
