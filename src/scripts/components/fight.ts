import { controls } from '../../constants/controls';
import { fighterText } from '../../constants/fighterText';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails> {
  const playerOne = createPlayer(firstFighter, 'left');
  const playerTwo = createPlayer(secondFighter, 'right');

  return new Promise((resolve): void => {
    const pressedKeys: Set<string> = new Set();
    document.addEventListener('keydown', keyDownEvent);
    document.addEventListener('keyup', keyUpEvent);

    function keyDownEvent(event: KeyboardEvent): void {
      pressedKeys.add(event.code);
      fightAction(playerOne, playerTwo, pressedKeys);
    }

    function keyUpEvent(event: KeyboardEvent): void {
      pressedKeys.delete(event.code);
      disableBlock(playerOne, playerTwo, event.code);
      const isGameOver = checkGameOver(playerOne, playerTwo);

      if (isGameOver) {
        document.removeEventListener('keydown', keyDownEvent);
        document.removeEventListener('keyup', keyUpEvent);
        const winner = playerOne.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      }
    }
  });
}

function createPlayer(fighter: IFighterDetails, side: string): IPLayer {
  return {
    ...fighter,
    currentHealth: fighter.health,
    block: false,
    canUseCombo: true,
    indicator: `#${side}-fighter-indicator`,
    comboStatus: `#${side}-fighter-combo-status`,
    fightText: `#${side}-fighter-text`,
  };
}

function fightAction(playerOne: IPLayer, playerTwo: IPLayer, pressedKeys: Set<string>): void {
  switch (true) {
    case pressedKeys.has(controls.PlayerOneAttack):
      attack(playerOne, playerTwo);
      break;
    case pressedKeys.has(controls.PlayerTwoAttack):
      attack(playerTwo, playerOne);
      break;
    case checkCombo(controls.PlayerOneCriticalHitCombination, pressedKeys):
      criticalHit(playerOne, playerTwo);
      break;
    case checkCombo(controls.PlayerTwoCriticalHitCombination, pressedKeys):
      criticalHit(playerTwo, playerOne);
      break;
    case pressedKeys.has(controls.PlayerOneBlock):
      playerOne.block = true;
      break;
    case pressedKeys.has(controls.PlayerTwoBlock):
      playerTwo.block = true;
      break;
  }
}

function changeHealthIndicator(player: IPLayer): void {
  const { health, currentHealth, indicator } = player;
  const playerIndicator = <HTMLElement>document.querySelector(indicator);
  const indicatorValue = (currentHealth / health) * 100;
  playerIndicator.style.width = indicatorValue > 0 ? indicatorValue + '%' : '0';
}

function criticalHit(attacker: IPLayer, defender: IPLayer): void {
  const playerFightText = <HTMLImageElement>document.querySelector(attacker.fightText);

  if (attacker.canUseCombo) {
    playerFightText.src = fighterText.comboImg;
    defender.currentHealth -= getCriticalHitPower(attacker);
    changeHealthIndicator(defender);
    frezzeCriticalHit(attacker);
  }
}

function frezzeCriticalHit(player: IPLayer): void {
  const { comboStatus } = player;
  const playerComboStatus = <HTMLElement>document.querySelector(comboStatus);
  playerComboStatus.style.visibility = 'hidden';
  player.canUseCombo = false;
  setTimeout(() => {
    playerComboStatus.style.visibility = 'visible';
    player.canUseCombo = true;
  }, 10000);
}

function attack(attacker: IPLayer, defender: IPLayer): void {
  const attackerFightText = <HTMLImageElement>document.querySelector(attacker.fightText);
  const defenderFightText = <HTMLImageElement>document.querySelector(defender.fightText);

  if (!attacker.block && !defender.block) {
    attackerFightText.src = fighterText.kickImg;
    defender.currentHealth -= getDamage(attacker, defender);
    changeHealthIndicator(defender);
  }
  if (defender.block) {
    defenderFightText.src = fighterText.blockImg;
  }
}

function checkCombo(combination: Array<string>, pressedKeys: Set<string>): boolean {
  for (const key of combination) {
    if (!pressedKeys.has(key)) {
      return false;
    }
  }
  return true;
}

function disableBlock(playerOne: IPLayer, playerTwo: IPLayer, key: string): void {
  switch (key) {
    case controls.PlayerOneBlock:
      playerOne.block = false;
      break;
    case controls.PlayerTwoBlock:
      playerTwo.block = false;
      break;
  }
}

function checkGameOver(playerOne: IPLayer, playerTwo: IPLayer): false | IPLayer {
  if (playerOne.currentHealth <= 0) {
    return playerTwo;
  } else if (playerTwo.currentHealth <= 0) {
    return playerOne;
  } else {
    return false;
  }
}

export function getDamage(attacker: IPLayer, defender: IPLayer): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: IPLayer): number {
  const criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IPLayer): number {
  const dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
}

function getCriticalHitPower({ attack }: IPLayer): number {
  return attack * 2;
}

function getRandom(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}
