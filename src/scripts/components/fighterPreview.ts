import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter: IFighterDetails, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const imgElement = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);

  fighterElement.append(imgElement, fighterInfo);
  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source, name } = fighter;

  const attributes = { src: source, title: name, alt: name };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter: IFighterDetails): HTMLElement {
  const fighterStatBlock = createElement({
    tagName: 'div',
    className: `fighter-preview__stats`,
  });
  const fighterName = createElement({
    tagName: 'div',
    className: `fighter-preview__name`,
  });
  fighterName.innerHTML = fighter.name;

  const health = createFighterStat('Health', fighter.health);
  const attack = createFighterStat('Attack', fighter.attack);
  const defense = createFighterStat('Defense', fighter.defense);

  fighterStatBlock.append(fighterName, health, attack, defense);
  return fighterStatBlock;
}

function createFighterStat(statName: string, statValue: number): HTMLElement {
  const fighterStat = createElement({
    tagName: 'div',
    className: `fighter-preview__stat`,
  });
  const statTag = createElement({
    tagName: 'span',
    className: `fighter-preview__tag`,
  });

  statTag.innerHTML = statName;
  fighterStat.append(statTag);
  fighterStat.innerHTML += ` ${statValue} points`;

  return fighterStat;
}
