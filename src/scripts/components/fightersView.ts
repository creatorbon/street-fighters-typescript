import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters: Array<IFighterDetails>): HTMLElement {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: IFighterDetails,
  selectFighter: (event: Event, fighterId: string) => Promise<void>
): HTMLElement {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event: Event): Promise<void> => {
    return selectFighter(event, fighter._id);
  };

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: IFighterDetails): HTMLElement {
  const { source, name } = fighter;
  const attributes = { src: source, title: name, alt: name };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  });

  return imgElement;
}
