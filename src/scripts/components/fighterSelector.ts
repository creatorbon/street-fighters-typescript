import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import versusImg from '../../../resources/versus.png';

export function createFightersSelector(): (event: Event, fighterId: string) => Promise<void> {
  let selectedFighters: Array<IFighterDetails> = [];

  return async (event: Event, fighterId: string): Promise<void> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo]: Array<IFighterDetails> = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = playerOne ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  const fighter = await fighterService.getFighterDetails(fighterId);

  return fighter;
}

function renderSelectedFighters(selectedFighters: Array<IFighterDetails>): void {
  if (selectedFighters[1]) {
    const fightersPreview = <HTMLElement>document.querySelector('.preview-container___root');
    const [playerOne, playerTwo] = selectedFighters;
    const firstPreview = createFighterPreview(playerOne, 'left');
    const secondPreview = createFighterPreview(playerTwo, 'right');
    const versusBlock = createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  }
}

function createVersusBlock(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: `${versusImg}` },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<IFighterDetails>): void {
  renderArena(selectedFighters);
}
