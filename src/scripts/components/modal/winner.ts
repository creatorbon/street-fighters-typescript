import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter: IFighterDetails): void {
  showModal({
    title: `${fighter.name} won!!!!`,
    bodyElement: createFighterImage(fighter),
  });
}
