import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import comboStatus from '../../../resources/text/combo-status.png';

export function renderArena(selectedFighters: Array<IFighterDetails>): void {
  const root = <HTMLElement>document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  const [firstFighter, secondFighter] = selectedFighters;
  fight(firstFighter, secondFighter).then((res) => showWinnerModal(res as IFighterDetails));
}

function createArena(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(...[leftFighter, rightFighter]: Array<IFighterDetails>): HTMLElement {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: string): HTMLElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });
  const combo = createElement({
    tagName: 'img',
    className: 'arena___fighter-combo-status',
    attributes: { src: `${comboStatus}`, id: `${position}-fighter-combo-status` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator, combo);

  return container;
}

function createFighters(...[firstFighter, secondFighter]: Array<IFighterDetails>): HTMLElement {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: string): HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });
  const fighterText = createElement({
    tagName: 'img',
    className: `fighter-preview___text`,
    attributes: {
      id: `${position}-fighter-text`,
    },
  });

  fighterElement.append(imgElement, fighterText);
  return fighterElement;
}
