import kickImg from '../../resources/text/kick.png';
import blockImg from '../../resources/text/block.png';
import comboImg from '../../resources/text/combo.png';

export const fighterText = {
  kickImg,
  blockImg,
  comboImg,
};
