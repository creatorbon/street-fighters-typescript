const presets = [
  [
    '@babel/preset-env',
    {
      targets: {
        firefox: '60',
        chrome: '67',
      },
    },
  ],
  '@babel/preset-typescript',
];

const plugins = ['@babel/plugin-proposal-class-properties'];

module.exports = {
  presets,
  plugins,
};
